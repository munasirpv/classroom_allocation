import 'package:http/http.dart' as http;

String apiKey = '7865F';
String apiUrl = 'https://hamon-interviewapi.herokuapp.com';

Future<http.Response> getData({String methodName, String id = ''}) async{
  return http.get('$apiUrl/$methodName/$id?api_key=$apiKey');
}