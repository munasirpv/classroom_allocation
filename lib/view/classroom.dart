import 'dart:convert';

import 'package:classroomallocation/general/api.dart';
import 'package:flutter/material.dart';

class Classrooms extends StatefulWidget {
  @override
  _ClassroomsState createState() => _ClassroomsState();
}

class _ClassroomsState extends State<Classrooms> {
  List classroomList = [];
  bool isLoading = true;
  @override
  void initState() {
    super.initState();
    getClassrooms();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Classrooms'),
      ),
      body: isLoading ? Center(child: CircularProgressIndicator()) : ListView.builder(
        itemCount: classroomList.length,
        itemBuilder: (BuildContext context, int index){
          return ListTile(
            title: Text(classroomList[index]['name']),
            onTap: () => showClassroomDetails(classroomList[index]['id'].toString()),
          );
        },
      ),
    );
  }

  getClassrooms() async{
    await getData(methodName: 'classrooms').then((response) {
      if(response.statusCode == 200){
        setState(() {
          classroomList = json.decode(response.body)['classrooms'];
          isLoading = false;
        });
      }
    });
  }

  showClassroomDetails(String id){
    showDialog(
      context: context,
      builder: (BuildContext context){
        return ClassroomDetails(id: id);
      }
    );
  }

}

class ClassroomDetails extends StatefulWidget {
  String id;
  ClassroomDetails({this.id});
  @override
  _ClassroomDetailsState createState() => _ClassroomDetailsState();
}

class _ClassroomDetailsState extends State<ClassroomDetails> {
  bool isLoading = true;
  Map classroomData = {};
  @override
  void initState() {
    super.initState();
    getClassroomDetails();
  }
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: isLoading ? Container(
        height: 50,
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      ) : Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Name: ${classroomData['name']}'
          ),
          Text(
            'Layout: ${classroomData['layout']}'
          ),
          Text(
            'Size: ${classroomData['size'].toString()}'
          ),
          Text(
            'Subject: ${classroomData['subject']}'
          ),
        ],
      ),
      actions: [
        FlatButton(
          child: Text('Okay'),
          onPressed: () => Navigator.pop(context),
        )
      ],
    );
  }

  getClassroomDetails() async{
    await getData(methodName: 'classrooms', id: widget.id).then((response) {
      if(response.statusCode == 200){
        setState(() {
          classroomData = json.decode(response.body);
          isLoading = false;
        });
      }
    });
  }

}


