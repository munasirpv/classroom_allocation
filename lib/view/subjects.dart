import 'dart:convert';

import 'package:classroomallocation/general/api.dart';
import 'package:flutter/material.dart';

class Subjects extends StatefulWidget {
  @override
  _SubjectsState createState() => _SubjectsState();
}

class _SubjectsState extends State<Subjects> {
  List subjectList = [];
  bool isLoading = true;
  @override
  void initState() {
    super.initState();
    getSubjects();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Subjects'),
      ),
      body: isLoading ? Center(child: CircularProgressIndicator()) : ListView.builder(
        itemCount: subjectList.length,
        itemBuilder: (BuildContext context, int index){
          return ListTile(
            title: Text(subjectList[index]['name']),
            onTap: () => showSubjectDetails(subjectList[index]),
          );
        },
      ),
    );
  }

  getSubjects() async{
    await getData(methodName: 'subjects').then((response) {
      if(response.statusCode == 200){
        setState(() {
          subjectList = json.decode(response.body)['subjects'];
          isLoading = false;
        });
      }
    });
  }

  showSubjectDetails(Map subjectData){
    showDialog(
        context: context,
        builder: (BuildContext context){
          return AlertDialog(
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Subject: ${subjectData['name']}'),
                Text('Teacher: ${subjectData['teacher']}'),
                Text('Credits: ${subjectData['credits'].toString()}'),
              ],
            ),
            actions: [
              FlatButton(
                child: Text('Okay'),
                onPressed: () => Navigator.pop(context),
              )
            ],
          );
        }
    );
  }
}
