import 'dart:convert';

import 'package:classroomallocation/general/api.dart';
import 'package:flutter/material.dart';

class Students extends StatefulWidget {
  @override
  _StudentsState createState() => _StudentsState();
}

class _StudentsState extends State<Students> {
  List studentList = [];
  bool isLoading = true;
  @override
  void initState() {
    super.initState();
    getStudents();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Students'),
      ),
      body: isLoading ? Center(child: CircularProgressIndicator()) : ListView.builder(
        itemCount: studentList.length,
        itemBuilder: (BuildContext context, int index){
          return ListTile(
            title: Text(studentList[index]['name']),
            onTap: () => showStudentDetails(studentList[index]),
          );
        },
      ),
    );
  }

  getStudents() async{
    await getData(methodName: 'students').then((response) {
      if(response.statusCode == 200){
        setState(() {
          studentList = json.decode(response.body)['students'];
          isLoading = false;
        });
      }
    });
  }

  showStudentDetails(Map studentData){
    showDialog(
      context: context,
      builder: (BuildContext context){
        return AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Name: ${studentData['name']}'),
              Text('Age: ${studentData['age'].toString()}'),
              Text('Email: ${studentData['email']}'),
            ],
          ),
          actions: [
            FlatButton(
              child: Text('Okay'),
              onPressed: () => Navigator.pop(context),
            )
          ],
        );
      }
    );
  }

}
